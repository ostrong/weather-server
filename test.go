package main

import (
  "math"
  "fmt"
)

func round(val float64, prec int) float64 {
  r := int (val * math.Pow10(prec+1)) % 10
  n := math.Abs(float64(int (val * math.Pow10(prec))))
  sign := 1
  if val < 0 {
    sign = -1
  }

  if math.Abs(float64(r)) >= 5 {
    n++
  }
  x := n / math.Pow10(prec) * float64(sign)
  return x
}

func main () {
  fmt.Println (round (-2.567, 1))
}
