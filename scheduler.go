package notify

import (
	"./weather-server"
	"fmt"
)

type NotificationRequest struct {
	lat       float64 `json:"lat"       bson:"lat"`
	lon       float64 `json:"lon"       bson:"lon"`
	zip       string  `json:"zip"       bson:"zip"`
	use_zip   bool    `json:"use_zip"   bson:"use_zip"`
	uid       float64 `json:"uid"       bson:"uid"`
	trigger   string  `json:"trigger"   bson:"trigger"`
	last_sent int64   `json:"last_sent" bson:"last_sent"`
}

type Alert struct {
	NR    NotificationRequest `json:"NR" bson:"NR"`
	msg   string              `json:"msg" bson:"msg"`
	title string              `json:"title" bson:"title"`
	to    string              `json:"to" bson:"to"`
}

func main() {
	alerts := make(chan Alert)

	fmt.Println("ow")
	return nil
}
