package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"strconv"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	//"fmt"
	"github.com/go-martini/martini"
	"github.com/martini-contrib/cors"
	"github.com/martini-contrib/secure"
)

/******************************************************************************/

type NotificationRequest struct {
	lat       float64 `json:"lat"       bson:"lat"`
	lon       float64 `json:"lon"       bson:"lon"`
	zip       string  `json:"zip"       bson:"zip"`
	use_zip   bool    `json:"use_zip"   bson:"use_zip"`
	uid       float64 `json:"uid"       bson:"uid"`
	trigger   string  `json:"trigger"   bson:"trigger"`
	last_sent int64   `json:"last_sent" bson:"last_sent"`
}

type Alert struct {
	NR    NotificationRequest `json:"NR" bson:"NR"`
	msg   string              `json:"msg" bson:"msg"`
	title string              `json:"title" bson:"title"`
	to    string              `json:"to" bson:"to"`
}

func (nr *NotificationRequest) Put(c *mgo.Collection) error {
	err := c.Insert(nr)
	return err
}

func (nr *NotificationRequest) Get(c *mgo.Collection) error {
	err := c.Find(bson.M{"uid": nr.uid}).Sort("-dt").One(&nr)
	return err
}

/******************************************************************************/

/******************************************************************************/
type AppFault struct {
	Data    string `json:"data"	bson:"data"`
	Status  string `json:"status" bson:"status"`
	Headers string `json:"headers" bson:"headers"`
	Config  string `json:"config"	bson:"config"`
	Dt      int64  `json:"dt"	bson:"dt"`
}

// will want to make this valid before using
func (af *AppFault) Get(c *mgo.Collection) error {
	err := c.Find(bson.M{"data": af.Data}).Sort("-dt").One(&af)
	return err
}

func (af AppFault) Put(c *mgo.Collection) error {
	err := c.Insert(af)
	return err
}

/******************************************************************************/

type WeatherConditions struct {
	Coord   Coord     `json:"coord"     bson:"coord"`
	Sys     Sys       `json:"sys"       bson:"sys"`
	Weather []Weather `json:"weather"   bson:"weather"`
	Base    string    `json:"base"      bson:"base"`
	Main    Main      `json:"main"      bson:"main"`
	Wind    Wind      `json:"wind"      bson:"wind"`
	Clouds  Clouds    `json:"clouds"    bson:"clouds"`
	Dt      int64     `json:"dt"        bson:"dt"`
	Id      float64   `json:"id"        bson:"id"`
	Name    string    `json:"name"      bson:"name"`
	Cod     float64   `json:"cod"       bson:"cod"`
	Calcs   Calcs     `json:"calcs"     bson:"calcs"`
}

type Coord struct {
	Lon float64 `json:"lon"        bson:"lon"`
	Lat float64 `json:"lat"        bson:"lat"`
}

type Sys struct {
	Message float64 `json:"message"    bson:"message"`
	Country string  `json:"country"    bson:"country"`
	Sunrise float64 `json:"sunrise"    bson:"sunrise"`
	Sunset  float64 `json:"sunset"     bson:sunset`
	Pod     string  `json:"pod"        bson:"pod"`
}

type Weather struct {
	Id          float64 `json:"id"          bson:"id"`
	MainConds   string  `json:"main"        bson:"main"`
	Description string  `json:"description" bson:"description"`
	Icon        string  `json:"icon"        bson:"icon"`
}

type Main struct {
	Temp        float64 `json:"temp"        bson:"temp"`
	TempMin     float64 `json:"temp_min"    bson:"temp_min"`
	TempMax     float64 `json:"temp_max"    bson:"temp_max"`
	Pressure    float64 `json:"pressure"    bson:"pressure"`
	SeaLevel    float64 `json:"sea_level"   bson:"sea_level"`
	GroundLevel float64 `json:"grnd_level"  bson:"grnd_level"`
	Humidity    float64 `json:"humidity"    bson:"humidity"`
	TempKf      float64 `json:"temp_kf"     bson:"temp_kf"`
}

type Wind struct {
	Speed   float64 `json:"speed"       bson:"speed"`
	Degrees float64 `json:"deg"         bson:"deg"`
	Gust    float64 `json:"gust"        bson:"gust"`
}

type Clouds struct {
	All float64 `json:"all"         bson:"all"`
}

/*
 * Custom struct for our own calculations
 * Not part of OpenWeather API
 */
type Calcs struct {
	WB     float64 `json:"wb"          bson:"wb"`
	Recent int64   `json:"recent"      bson:"recent"`
}

type City struct {
	Id         int64   `json:"id"          bson:"id"`
	Name       string  `json:"name"        bson:"name"`
	Coord      Coord   `json:"coord"       bson:"coord"`
	Country    string  `json:"country"     bson:"country"`
	Population float64 `json:"population"  bson:"population"`
}

type Rain struct {
	H3 float64 `json:"3h"          bson:"3h"`
}

type Snow struct {
	H3 float64 `json:"3h"          bson:"3h"`
}

type List struct {
	Dt      int64     `json:"dt"        bson:"dt"`
	Main    Main      `json:"main"      bson:"main"`
	Weather []Weather `json:"weather"   bson:"weather"`
	Clouds  Clouds    `json:"clouds"    bson:"clouds"`
	Wind    Wind      `json:"wind"      bson:"wind"`
	Rain    Rain      `json:"rain"      bson:"rain"`
	DtTxt   string    `json:"dt_txt"    bson:"dt_txt"`
	Calcs   Calcs     `json:"calcs"     bson:"calcs"`
}

type Forecast struct {
	Cod     string  `json:"cod"       bson:"cod"`
	Message float64 `json:"message"   bson:"message"`
	City    City    `json:"city"      bson:"city"`
	Cnt     int64   `json:"cnt"       bson:"cnt"`
	List    []List  `json:"list"      bson:"list"`
}

type API interface {
	Fetch() error
	BuildRequest() string
}

type Mongo interface {
	Get(c mgo.Collection) error
	Put(c mgo.Collection) error
}

// Methods required for the API interface
func (wc *WeatherConditions) Fetch() error {
	request := API.BuildRequest(wc)
	resp, err := http.Get(request)
	//fmt.Println("herro")
	//fmt.Println(ioutil.ReadAll (resp.Body))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	s, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(s, &wc)
	if err != nil {
		return err
	}

	return nil
}

func (wc *WeatherConditions) BuildRequest() string {
	var requestURL string = "http://api.openweathermap.org/data/2.5/weather"
	var apiKey string = "&id=ee6e872f766992de803c96b68757fb70" // Yes, I know this doesn't belong here
	return requestURL + "?lat=" + strconv.FormatFloat(wc.Coord.Lat, 'f', 4, 64) + "&lon=" + strconv.FormatFloat(wc.Coord.Lon, 'f', 4, 64) + apiKey
}

// Methods required for the Mongo Interface
func (wc *WeatherConditions) Get(c *mgo.Collection) error {
	// this is where the record is fetched, I know I will overlook this line and
	// wonder where the result is coming from, the answer is here
	err := c.Find(bson.M{"coord.lat": round(wc.Coord.Lat, 2), "coord.lon": round(wc.Coord.Lon, 2)}).Sort("-dt").One(&wc)
	// the weather for the location is not in mongo
	if err == mgo.ErrNotFound {
		//fmt.Println("not in db")
		err = wc.Fetch()
		if err != nil {
			return err
		}
		wc.CalcWetBulb()
		err = wc.Put(c)
		return err
	}
	// something else bad
	if err != nil {
		//fmt.Println("something bad")
		return err
	}
	// found something, but it is not current
	if wc.IsRecent() {
		//fmt.Println("recent record")
		return nil
	} else { // get a fresh result
		//fmt.Println("not recent")
		err = wc.Fetch()
		if err != nil {
			return err
		}
		wc.CalcWetBulb()
		err = wc.Put(c)
		return err
	}
	return nil // should never get here
}

func (wc *WeatherConditions) WeatherResponse(c *mgo.Collection) string {
	err := wc.Get(c)
	if err == nil {
		resp, err := json.Marshal(wc)
		if err == nil {
			return string(resp)
		}
	}
	return ""
}

func (wc *WeatherConditions) Put(c *mgo.Collection) error {
	err := c.Insert(wc)
	return err
}

func (w *WeatherConditions) CalcWetBulb() {
	w.Calcs.WB = WBfromTRH(w.Main.Temp, w.Main.Humidity)
}

func (wc *WeatherConditions) IsRecent() bool {
	// hardcoded this, for now
	if (time.Now().Unix() - wc.Dt) < 1800 {
		return true
	}

	return false
}

/******************************************************************************/
//---------------------------- Start Forecasting Methods -----------------------
/******************************************************************************/

func (f *Forecast) ForecastResponse(c *mgo.Collection) string {
	err := f.Get(c)
	//fmt.Println(f)
	if err == nil {
		resp, err := json.Marshal(f)
		if err == nil {
			return string(resp)
		}
	}
	//fmt.Println ("helllllllllll0")
	return ""
}

func (f *Forecast) Put(c *mgo.Collection) error {
	err := c.Insert(f)
	return err
}

func (f *Forecast) CalcWetBulb() {
	for i, _ := range f.List {
		//fmt.Println(i)
		f.List[i].Calcs.WB = WBfromTRH(f.List[i].Main.Temp, f.List[i].Main.Humidity)
	}
}

func (f *Forecast) IsRecent() bool {
	if len(f.List) > 0 {
		if time.Now().Unix() < f.List[0].Dt {
			return true
		}
	}

	return false
}

func (f *Forecast) Get(c *mgo.Collection) error {
	// this is where the record is fetched, I know I will overlook this line and
	// wonder where the result is coming from, the answer is here
	err := c.Find(bson.M{"City.Coord.Lat": round(f.City.Coord.Lat, 2), "City.Coord.Lon": round(f.City.Coord.Lon, 2)}).Sort("-dt").One(&f)
	// the weather for the location is not in mongo
	if err == mgo.ErrNotFound {
		//fmt.Println("not in db")
		err = f.Fetch()
		if err != nil {
			return err
		}
		f.CalcWetBulb()
		err = f.Put(c)
		return err
	}
	// something else bad
	if err != nil {
		//fmt.Println("see below")
		//fmt.Println(err)
		return err
	}
	// found something, but it is not current
	if f.IsRecent() {
		//fmt.Println("recent record")
		return nil
	} else { // get a fresh result
		//fmt.Println("not recent")
		err = f.Fetch()
		if err != nil {
			return err
		}
		f.CalcWetBulb()
		err = f.Put(c)
		return err
	}
	return nil // should never get here
}

// Methods required for the API interface
func (f *Forecast) Fetch() error {
	request := API.BuildRequest(f)
	resp, err := http.Get(request)
	////fmt.Println("herro")
	////fmt.Println(ioutil.ReadAll (resp.Body))
	if err != nil {
		////fmt.Println(err)
		return err
	}
	defer resp.Body.Close()

	s, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//fmt.Println(err)
		return err
	}
	err = json.Unmarshal(s, &f)
	if err != nil {
		//fmt.Println(err)
		return err
	}

	return nil
}

func (f *Forecast) BuildRequest() string {
	var requestURL string = "http://api.openweathermap.org/data/2.5/forecast"
	var apiKey string = "&id=ee6e872f766992de803c96b68757fb70" // Yes, I know this doesn't belong here
	return requestURL + "?lat=" + strconv.FormatFloat(f.City.Coord.Lat, 'f', 4, 64) + "&lon=" + strconv.FormatFloat(f.City.Coord.Lon, 'f', 4, 64) + apiKey
}

/******************************************************************************/
//---------------------------- Start Utility Methods ---------------------------
/******************************************************************************/

// Methods required for Weather interface
// https://wiki.engr.illinois.edu/download/attachments/45842726/Stull+2011+-+Wet+Bulb+from+RH+and+T.pdf?version=1&modificationDate=1359651680000
// temperature in Kelvin
// Wetbulb reported in degrees F
// relative humidity as percent : rh : 65.8% = 65.8
func WBfromTRH(t float64, rh float64) float64 {
	tf := (t-273.15)*1.8 + 32
	return tf*math.Atan(.151977*math.Pow((rh+8.313659), .5)) + math.Atan(tf+rh) - math.Atan(rh-1.676331) + .00391838*math.Pow(rh, 1.5)*math.Atan(.023101*rh) - 4.686035
}

//worst rounding function ever?
func round(val float64, prec int) float64 {
	// Get number after cut off
	r := int(val*math.Pow10(prec+1)) % 10
	// Get the absolute value of unrounded number with no decimal
	n := math.Abs(float64(int(val * math.Pow10(prec))))
	// determine if original is positive or negative
	sign := 1
	if val < 0 {
		sign = -1
	}

	// if digit after rounding is 5 or bigger round up
	if math.Abs(float64(r)) >= 5 {
		n++
	}
	// reinsert decimal
	x := n / math.Pow10(prec) * float64(sign)
	return x
}

type MSG struct {
	Suggestion string  `json:"suggestion"      bson:"suggestion"`
	WB_Max     float64 `json:"wb_max"          bson:"wb_max"`
	WB_Min     float64 `json:"wb_min"          bson:"wb_min"`
	Color      string  `json:"color"           bson:"color"`
}

// UNTESTED
func MSGResponse(wb float64, c *mgo.Collection) string {
	var msg MSG
	err := c.Find(bson.M{"wb_min": bson.M{"$lt": wb}, "wb_max": bson.M{"$gt": wb}}).Sort("-$natural").One(&msg)
	if err == mgo.ErrNotFound {
		return ""
	} else if err != nil {
		return ""
	}
	resp, err := json.Marshal(msg)
	if err != nil {
		return ""
	}
	return string(resp)
}

var m *martini.Martini

func main() {
	// connect to mongo
	// in production we should probably have at least one redundant node
	var collection *mgo.Collection
	session, err := mgo.Dial("chpappdev.cmich.edu")
	if err != nil {
		panic(err)
	}
	defer session.Close()
	//connect to the right db
	db := session.DB("weather")
	//authenticate
	err = db.Login("weatherRPC", "weatherRPC")
	if err != nil {
		panic(err)
	}
	collection = db.C("weather")

	// connect to our error storage
	var apperrors *mgo.Collection
	session, err = mgo.Dial("chpappdev.cmich.edu")
	if err != nil {
		panic(err)
	}
	defer session.Close()
	db = session.DB("errorlog")
	err = db.Login("errorlog", "errorlog")
	if err != nil {
		panic(err)
	}
	apperrors = db.C("errorlog")

	//start the server things
	martini.Env = martini.Prod
	m = martini.New()
	m.Use(martini.Recovery())
	m.Use(martini.Logger())
	// Allow ALL the origins!
	m.Use(cors.Allow(&cors.Options{
		AllowOrigins: []string{"*"},
	}))

	r := martini.NewRouter()
	r.Get("/weather/:lat/:lon", func(params martini.Params) string {
		// set some defaults so we can dump errors
		var x WeatherConditions
		x.Coord.Lon = 43.5978
		x.Coord.Lat = 84.7675
		// disregard strconv errors, acquire weather
		x.Coord.Lon, _ = strconv.ParseFloat(params["lon"], 64)
		x.Coord.Lat, _ = strconv.ParseFloat(params["lat"], 64)
		//fmt.Println(x.WeatherResponse(collection))
		return x.WeatherResponse(collection)
	})

	r.Get("/forecast/:lat/:lon", func(params martini.Params) string {
		var f Forecast
		// disregard strconv errors, acquire weather
		f.City.Coord.Lon, err = strconv.ParseFloat(params["lon"], 64)
		if err != nil {
			f.City.Coord.Lon = 43.5978
		}
		f.City.Coord.Lat, err = strconv.ParseFloat(params["lat"], 64)
		if err != nil {
			f.City.Coord.Lat = 84.7675
		}
		////fmt.Println(f.ForecastResponse(collection))
		return f.ForecastResponse(collection)
	})

	r.Get("/msg/:wb", func(params martini.Params) string {
		var wb float64
		wb = 99
		wb, _ = strconv.ParseFloat(params["wb"], 64)
		return MSGResponse(wb, collection)
	})

	r.Get("/error/:data/:status/:headers/:config/:dt", func(params martini.Params) (int, string) {
		var af AppFault
		af.Data = params["data"]
		af.Status = params["status"]
		af.Headers = params["headers"]
		af.Config = params["config"]
		// meh time isn't that important
		af.Dt, _ = strconv.ParseInt(params["Dt"], 10, 64)
		err := af.Put(apperrors)
		if err != nil {
			return 418, "something bad happened"
		}
		return 200, "ok"
	})

	m.Action(r.Handle)

	m.Use(secure.Secure(secure.Options{
		SSLRedirect: true,
		SSLHost:     "localhost:8443", // This is optional in production. The default behavior is to just redirect the request to the https protocol. Example: http://github.com/some_page would be redirected to https://github.com/some_page.
	}))

	// HTTP
	//go func() {
	//		if err := http.ListenAndServe(":8080", m); err != nil {
	//			log.Fatal(err)
	//		}
	//}()

	// HTTPS
	// To generate a development cert and key, run the following from your *nix terminal:
	// go run $GOROOT/src/pkg/crypto/tls/generate_cert.go --host="localhost"
	if err := http.ListenAndServeTLS(":8443", "chpappdevcert.txt", "chpappdev_cmich_edu.key", m); err != nil {
		log.Fatal(err)
	}

}
