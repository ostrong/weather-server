package main

import (
  "encoding/json"
  "net/http"
  "bytes"
  "fmt"
)

type GCM struct {
  ApiKey    string
  ApiUrl    string
}

type APN struct {
  ApiUrl    string
  Pk        string
  Password  string
}


func (g *GCM) push (regId string, msg []string, collapse_key string) error {
  type payload struct {
    registration_id     string      `json:"registration_id"`
    collapse_key        string      `json:"collapse_key"`
    data                []string    `json:"data"`
    delay_while_idle    bool        `json:"delay_while_idle"`
  }

  p := new(payload)
  p.registration_id = regId
  p.collapse_key = collapse_key
  p.data = msg
  p.delay_while_idle = true

  j_payload, err := json.Marshal (p)
  req, err := http.NewRequest("POST", g.ApiUrl, bytes.NewBuffer(j_payload))
  req.Header.Set("Authorization: key=", g.ApiKey)
  req.Header.Set("Content-Type", "application/json")

  client := &http.Client{}
  resp, err := client.Do(req)
  if err != nil {
    fmt.Println(err)
    panic(err)
  }
  defer resp.Body.Close()

  return nil
}

func (a APN) push (alert string, badge int64, sound string, content-available int64) {
  type aps struct {
    alert               string      `json:"alert"`
    badge               int64       `json:"badge"`
    sound               string      `json:"sound"`
    content-available   int64       `json:"content-available"`
  }

  p := new(aps)
  p.alert = alert
  p.badge = badge
  p.sound = sound
  p.content-available = content-available

  j_aps, err := json.Marshal(p)
  
}


func main () {
  g := new(GCM)
  g.ApiKey = "key"
  g.ApiUrl = "http://www.squirrels.com"
  msg := [...]string{"1", "2"}
  k := append(msg[0:])
  _ = g.push ("regid", k, "collapse")
}
