package main

import (
  "net/http"
  "encoding/json"
  "io/ioutil"
  "time"
  "gopkg.in/mgo.v2"
  "gopkg.in/mgo.v2/bson"
  "strconv"
  "math"
  "fmt"
  "github.com/go-martini/martini"
  //"github.com/martini-contrib/cors"
)


type WeatherConditions struct {
  Coord         Coord       `json:"coord"     bson:"coords"`
  Sys           Sys         `json:"sys"       bson:"sys"`
  Weather       []Weather   `json:"weather"   bson:"weather"`
  Base          string      `json:"base"      bson:"base"`
  Main          Main        `json:"main"      bson:"main"`
  Wind          Wind        `json:"wind"      bson:"wind"`
  Clouds        Clouds      `json:"clouds"    bson:"clouds"`
  Dt            int64       `json:"dt"        bson:"dt"`
  Id            float64     `json:"id"        bson:"id"`
  Name          string      `json:"name"      bson:"name"`
  Cod           float64     `json:"cod"       bson:"cod"`
  Calcs         Calcs       `json:"calcs"     bson:"calcs"`
}

type Coord struct {
  Lon           float64    `json:"lon"        bson:"lon"`
  Lat           float64    `json:"lat"        bson:"lat"`
}

type Sys struct {
  Message       float64    `json:"message"    bson:"message"`
  Country       string     `json:"country"    bson:"country"`
  Sunrise       float64    `json:"sunrise"    bson:"sunrise"`
  Sunset        float64    `json:"sunset"     bson:sunset`
}

type Weather struct {
  Id            float64   `json:"id"          bson:"id"`
  MainConds     string    `json:"main"        bson:"main"`
  Description   string    `json:"description" bson:"description"`
  Icon          string    `json:"icon"        bson:"icon"`
}

type Main struct {
  Temp         float64    `json:"temp"        bson:"temp"`
  TempMin      float64    `json:"temp_min"    bson:"temp_min"`
  TempMax      float64    `json:"temp_max"    bson:"temp_max"`
  Pressure     float64    `json:"pressure"    bson:"pressure"`
  SeaLevel     float64    `json:"sea_level"   bson:"sea_level"`
  GroundLevel  float64    `json:"grnd_level"  bson:"grnd_level"`
  Humidity     float64    `json:"humidity"    bson:"humidity"`
}

type Wind struct {
  Speed        float64    `json:"speed"       bson:"speed"`
  Degrees      float64    `json:"deg"         bson:"deg"`
}

type Clouds struct {
  All          float64    `json:"all"         bson:"all"`
}

type Calcs struct {
  WB          float64     `json:"wb"          bson:"wb"`
  Recent      int64       `json:"recent"      bson:"recent"`
}

// Methods required for the API interface
func (wc *WeatherConditions) Fetch () error {
  request := API.BuildRequest(wc)
  resp, err := http.Get(request)
  if err != nil {
    return err
  }
  defer resp.Body.Close()

  s, err := ioutil.ReadAll (resp.Body)
  if err != nil {
    return err
  }
  err = json.Unmarshal(s, &wc)
  if (err != nil) {
    return err
  }

  return nil
}

func (wc *WeatherConditions) BuildRequest () string {
  var requestURL string = "http://api.openweathermap.org/data/2.5/weather"
  var apiKey string = "&id=ee6e872f766992de803c96b68757fb70" // Yes, I know this doesn't belong here
  return requestURL + "?lat=" + strconv.FormatFloat(wc.Coord.Lat, 'f', 4, 64) + "&lon=" + strconv.FormatFloat(wc.Coord.Lon, 'f', 4, 64) + apiKey
}


//worst rounding function ever?
func round(val float64, prec int) float64 {
  // Get number after cut off
  r := int (val * math.Pow10(prec+1)) % 10
  // Get the absolute value of unrounded number with no decimal
  n := math.Abs(float64(int (val * math.Pow10(prec))))
  // determine if original is positive or negative
  sign := 1
  if val < 0 {
    sign = -1
  }

  // if digit after rounding is 5 or bigger round up
  if math.Abs(float64(r)) >= 5 {
    n++
  }
  // reinsert decimal
  x := n / math.Pow10(prec) * float64(sign)
  return x
}


// Methods required for the Mongo Interface
func (wc *WeatherConditions) Get (c *mgo.Collection) error {
  // this is where the record is fetched, I know I will overlook this line and
  // wonder where the result is coming from, the answer is here
  err := c.Find(wc).Sort("-Dt").One(&wc)
  if err == mgo.ErrNotFound {
    err = wc.Fetch()
    wc.CalcWetBulb ()
  }
  if err != nil{
    return err
  }
  if (wc.IsRecent()) {
    return nil
  } else {
    err = mgo.ErrNotFound
  }
  return nil
}

func (wc *WeatherConditions) WeatherResponse (c *mgo.Collection) string {
  err := wc.Get(c)
  if err == nil {
    resp, err := json.Marshal(wc)
    if err == nil {
      return string(resp)
    }
  }
  return ""
}


func (wc *WeatherConditions) Put (c mgo.Collection) error {
  err := c.Insert(wc)
  return err
}

// Methods required for Weather interface
// https://wiki.engr.illinois.edu/download/attachments/45842726/Stull+2011+-+Wet+Bulb+from+RH+and+T.pdf?version=1&modificationDate=1359651680000
// temperature in Kelvin
// Wetbulb reported in degrees F
// relative humidity as percent : rh : 65.8% = 65.8
func (w *WeatherConditions) CalcWetBulb () {
  tf := (w.Main.Temp - 273.15) * 1.8 + 32
  w.Calcs.WB = tf*math.Atan(.151977*math.Pow((w.Main.Humidity+8.313659), .5)) + math.Atan(tf+w.Main.Humidity) - math.Atan(w.Main.Humidity-1.676331) + .00391838*math.Pow(w.Main.Humidity, 1.5)*math.Atan(.023101*w.Main.Humidity) - 4.686035
}


func (wc *WeatherConditions) IsRecent () bool {
  if (time.Now().Unix() - wc.Dt) < wc.Calcs.Recent {
    return true
  }

  return false
}

type MSG struct {
  Suggestion          string      `json:"suggestion"      bson:"suggestion"`
  WB_Max              float64     `json:"wb_max"          bson:"wb_max"`
  WB_Min              float64     `json:"wb_min"          bson:"wb_min"`
  Color               string      `json:"color"           bson:"color"`
}

/*func Warnings2Mongo () {
  white := new MSG ()
  white.Suggestion = "Normal Activity. Drink throughout the day for full hyrdration."
  white.WB_Min = -999
  white.WB_Max = 74.999999
  white.Color = "white"

  green := new MSG ()
  green.Suggestion = "Normal Activity. Take longer rest periods in the shade. Drink 3 - 9 oz every 20 minutes of activity. Were weather appropriate clothing."
  green.WB_Min = 75
  green.WB_Max = 79
  green.Color = "green"

  yellow := new MSG ()
  yellow.Suggestion = "Limit intense excercise to 2 hours, limit total outdoor excercise to 4 hours. Use discretion in planning intense physical activity. Light summer-weight clothing. Hydrate regularly 3-9 ounces every 20 minutes activity. Pay special attention to at risk individuals"
  yellow.WB_Min = 79.00000001
  yellow.WB_Max = 82
  yellow.Color = "yellow"

  red := new MSG ()
  red.Suggestion = "Limit intense excercise to 1 hour, limit total outdoor excercise to 3 hours. Rest after 15 to 20 minutes of activity. Hydrate regularly 3-9 ounces every 20 minutes of activity. Wear as little clothing as possible and remove excess equipment, which increases body heat to body. Be on high alert: watch for early signs and symptoms. Consider moving activity to early or late in the day."
  red.WB_Min = 72.00000001
  red.WB_Max = 85
  red.Color = "red"

  black := new MSG ()
  black.Suggestion = "Cancel all outdoor excercise involving physical excertion. Practice should be held in an airconditioned space. If the event cannot be cancelled, postponed or rescheduled then use extreme caution becaue the risk  of heat illness is very high. Closely monitor for signs and symptoms of heat illness and withdraw any athletes who show signs or symptoms."
  black.WB_Min = 85.00000001
  black.WB_Max = 999
  black.Color = "black"

  white.Put
} */

func (msg MSG) Put (c mgo.Collection) error {
  err := c.Insert(msg)
  return err
}

// UNTESTED
func MSGResponse (wb float64, c *mgo.Collection) string {
  var msg MSG
  err := c.Find(bson.M{"WB_Min": bson.M{"$lt": wb}, "WB_Max": bson.M{"$gt": wb}}).One(&msg)
  if err == mgo.ErrNotFound {
    fmt.Println("not found")
    return ""
  } else if err != nil{
    fmt.Println(err)
    return ""
  }
  fmt.Println(msg.Suggestion)
  return msg.Suggestion
}


type API interface {
  Fetch () error
  BuildRequest () string
}

type Mongo interface {
  Get (c mgo.Collection) error
  Put (c mgo.Collection) error
}

var m *martini.Martini
func main () {



  // connect to mongo
  // in production we should probably have at least one redundant node
  var collection *mgo.Collection
  session, err := mgo.Dial("141.209.85.183")
  if err != nil {
    panic(err)
  }
  defer session.Close()
  //connect to the right db
  db := session.DB("weather")
  //authenticate
  err = db.Login("weatherRPC", "weatherRPC")
  if err != nil {
    panic (err)
  }
  collection = db.C("weather")

  white := new (MSG)
  white.Suggestion = "Normal Activity. Drink throughout the day for full hyrdration."
  white.WB_Min = -999
  white.WB_Max = 74.999999
  white.Color = "white"

  green := new  (MSG)
  green.Suggestion = "Normal Activity. Take longer rest periods in the shade. Drink 3 - 9 oz every 20 minutes of activity. Wear weather appropriate clothing."
  green.WB_Min = 75
  green.WB_Max = 79
  green.Color = "green"

  yellow := new  (MSG)
  yellow.Suggestion = "Limit intense excercise to 2 hours, limit total outdoor excercise to 4 hours. Use discretion in planning intense physical activity. Light summer-weight clothing. Hydrate regularly 3-9 ounces every 20 minutes activity. Pay special attention to at risk individuals"
  yellow.WB_Min = 79.00000001
  yellow.WB_Max = 82
  yellow.Color = "yellow"

  red := new (MSG )
  red.Suggestion = "Limit intense excercise to 1 hour, limit total outdoor excercise to 3 hours. Rest after 15 to 20 minutes of activity. Hydrate regularly 3-9 ounces every 20 minutes of activity. Wear as little clothing as possible and remove excess equipment, which increases body heat to body. Be on high alert: watch for early signs and symptoms. Consider moving activity to early or late in the day."
  red.WB_Min = 72.00000001
  red.WB_Max = 85
  red.Color = "red"

  black := new (MSG )
  black.Suggestion = "Cancel all outdoor excercise involving physical excertion. Practice should be held in an airconditioned space. If the event cannot be cancelled, postponed or rescheduled then use extreme caution becaue the risk  of heat illness is very high. Closely monitor for signs and symptoms of heat illness and withdraw any athletes who show signs or symptoms."
  black.WB_Min = 85.00000001
  black.WB_Max = 999
  black.Color = "black"

  err = white.Put (*collection)
  if err != nil {
    fmt.Println ("no white")
  }

  err = green.Put (*collection)
  if err != nil {
    fmt.Println ("no green")
  }

  err = yellow.Put (*collection)
  if err != nil {
    fmt.Println ("no yellow")
  }

  err = red.Put (*collection)
  if err != nil {
    fmt.Println ("no red")
  }

  err = black.Put (*collection)
  if err != nil {
    fmt.Println ("no black")
  }

}
